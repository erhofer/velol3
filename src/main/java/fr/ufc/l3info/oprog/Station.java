package fr.ufc.l3info.oprog;

import java.util.HashSet;
import java.util.Set;

public class Station {
    private String nom;
    private double latitude;
    private double longitude;
    private int capacite;
    private IVelo [] bornes;
    private IRegistre reg;
    private int nbVelo;

    public Station(String nom, double latitude, double longitude, int capacite){
        this.nom=nom;
        this.latitude=latitude;
        this.longitude=longitude;
        this.capacite=capacite;
        this.bornes = new IVelo[capacite];
    }

        public void setRegistre(IRegistre registre){
        this.reg = registre;
    }

    public String getNom(){
        return this.nom;
    }

    public int capacite(){
        return this.capacite;
    }

    public int nbBornesLibres(){
        int cpt = 0;
        for (int i = 0 ; i < capacite(); i++){
            if(this.bornes[i]==null){
                cpt++;
            }
        }
        return cpt;
    }

    public IVelo veloALaBorne(int b){
        if(b < 1 || b > capacite()){
            return null;
        }
        return this.bornes[b-1];
    }

    public IVelo emprunterVelo(Abonne a, int b){
        if (this.reg == null){
            return null;
        }
        if(a.estBloque()){
            return null;
        }
        if(this.reg.nbEmpruntsEnCours(a)>0){
            return null;
        }
        this.reg.emprunter( a, veloALaBorne(b), maintenant());
        return veloALaBorne(b);
    }

    public int arrimerVelo(IVelo v, int b){
        if (b < 1 || b > capacite() || v == null){
            return -1;
        }
        IVelo v1 = this.bornes[b-1];
        if (this.reg == null || v1!=null){
            return -2;
        }
        int retour = this.reg.retourner(v, maintenant());
        this.nbVelo++;
        this.bornes[b-1] = v;
        if (retour < 0) {
            System.out.println(retour);
            return -4;
        }
        return 0;
    }

    public void equilibrer(Set<IVelo> velos){
        Set<IVelo> velosARevise = new HashSet<>();
        Set<IVelo> velosAbime = new HashSet<>();
        int moitie = (int)Math.round(capacite()/2);
        for(int i = 0; i < capacite();i++){
            if(this.bornes[i]!=null && this.bornes[i].estAbime()){
                velosAbime.add(this.bornes[i]);
                this.bornes[i]=null;
                this.nbVelo--;
            }else if(this.bornes[i]!=null && this.bornes[i].prochaineRevision() <= 0){
                velosARevise.add(this.bornes[i]);
                this.bornes[i]=null;
                this.nbVelo--;
            }
        }
        if(this.nbVelo < moitie){
            for (int j = 0 ; j < velos.size() ; j++){
                if(this.bornes[j]==null && !velos.iterator().next().estAbime() && this.bornes[j].prochaineRevision() > 0) {
                    IVelo cpt = velos.iterator().next();
                    this.bornes[j] = cpt;
                    velos.remove(cpt);
                }
            }
        }
        if(this.nbVelo < moitie){
            for (int j = this.nbVelo ; j < velosARevise.size() ; j++){
                if(this.bornes[j]==null && !velos.iterator().next().estAbime() && this.bornes[j].prochaineRevision() > 0) {
                    IVelo cpt = velos.iterator().next();
                    this.bornes[j] = cpt;
                    velos.remove(cpt);
                }
            }
        }
        velos.addAll(velosARevise);
        velos.addAll(velosAbime);
    }

    public double distance(Station s){
        double earthRadius = 6371; //meters
        double dLat = Math.toRadians(s.latitude-this.latitude);
        double dLng = Math.toRadians(s.longitude-this.longitude);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(this.latitude)) * Math.cos(Math.toRadians(this.latitude)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    public long maintenant(){
        return System.currentTimeMillis();
    }
}
