package fr.ufc.l3info.oprog;

public class FabriqueVelo {
    private static FabriqueVelo fab;

    private FabriqueVelo(){}

    public static FabriqueVelo getInstance(){
        if (fab == null){
            fab = new FabriqueVelo();
        }
        return fab;
    }

    public IVelo construire(char t, String...options){

        IVelo velo = new Velo(t);

        boolean alu = true;
        boolean avant = true;
        boolean arriere = true;
        boolean disque = true;
        boolean elec = true;
        for(String option : options) {
            switch (option) {
                case "CADRE_ALUMINIUM":
                    if (alu) {
                        velo = new OptCadreAlu(velo);
                        alu = false;
                    }
                    break;
                case "SUSPENSION_AVANT":
                    if (avant) {
                        velo = new OptSuspensionAvant(velo);
                        avant = false;
                    }
                    break;
                case "SUSPENSION_ARRIERE":
                    if (arriere) {
                        velo = new OptSuspensionArriere(velo);
                        arriere = false;
                    }
                    break;
                case "FREINS_DISQUE":
                    if (disque) {
                        velo = new OptFreinsDisque(velo);
                        disque = false;
                    }
                    break;
                case "ASSISTANCE_ELECTRIQUE":
                    if (elec) {
                        velo = new OptAssistanceElectrique(velo);
                        elec = false;
                    }
                    break;
                default:
                    break;
            }
        }
        return velo;
    }
}
