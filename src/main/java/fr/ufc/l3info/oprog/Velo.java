package fr.ufc.l3info.oprog;

public class Velo implements IVelo{
    private char cadre;
    private double kilometre;
    private double avRevision;
    private boolean accroche;
    private boolean abime;
    private double tarif;


    public Velo(){
        this.cadre = 'm';
        this.kilometre = 0;
        this.avRevision = 0;
        this.abime = false;
        this.accroche = false;
        this.tarif = 2.0;
    }

    public Velo(char t){
        if (t == 'f' || t == 'F' || t == 'h' || t == 'H' ){
            this.cadre = t;
        }else{
            this.cadre = 'm';
        }
        this.kilometre = 0;
        this.avRevision = 0;
        this.abime = false;
        this.accroche = false;
        this.tarif = 2.0;
    }

    @Override
    public double kilometrage() {
        return this.kilometre;
    }

    @Override
    public double prochaineRevision() {
        return 500-this.avRevision;
    }

    @Override
    public void parcourir(double km) {
        if(km>=0 && !this.accroche){
            this.kilometre += km;
            this.avRevision += km;
        }
    }

    @Override
    public double tarif() {
        return this.tarif;
    }

    @Override
    public int decrocher() {
        if(this.accroche){
            this.accroche = false;
            return 0;
        }else {
            return -1;
        }
    }

    @Override
    public int arrimer() {
        if(this.accroche){
            return -1;
        }else {
            this.accroche = true;
            return 0;
        }
    }

    @Override
    public void abimer() {
        this.abime=true;
    }

    @Override
    public boolean estAbime() {
        return this.abime;
    }

    @Override
    public int reviser() {
        if (!this.accroche){
            this.avRevision=0;
            if (this.abime) {
                this.abime = false;
            }
            return 0;
        }else{
            return -1;
        }
    }

    @Override
    public int reparer() {
        if (!this.accroche){
            if (estAbime()){
                this.abime = false;
                return 0;
            }else{
                return -2;
            }
        }else{
            return -1;
        }
    }

    @Override
    public String toString() {
        String chaine = "Vélo cadre ";
        switch (this.cadre){
            case 'f' : chaine=chaine+"femme - ";
                break;
            case 'F' : chaine=chaine+"femme - ";
                break;
            case 'h' : chaine=chaine+"homme - ";
                break;
            case 'H' : chaine=chaine+"homme - ";
                break;
            default: chaine=chaine+"mixte - ";
                break;
        }
        double arrondi = (double) (Math.round(kilometrage()*10))/10;
        chaine=chaine+arrondi+" km";
        if (prochaineRevision() <= 0){
            chaine=chaine+" (révision nécessaire)";
        }
        return chaine;
    }
}
