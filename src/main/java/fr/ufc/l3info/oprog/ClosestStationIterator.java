package fr.ufc.l3info.oprog;

import org.w3c.dom.Node;

import java.util.Iterator;
import java.util.Set;

public class ClosestStationIterator implements Iterator<Station> {

    Station stationDepart;
    Set<Station> stationsDispo;
    double distance;
    Station stationDistance;

    public ClosestStationIterator(Set<Station> stations, Station s){
        stationDepart = s;
        this.stationsDispo = stations;
        stationsDispo.remove(s);
    }


    @Override
    public boolean hasNext() {
        if (stationsDispo.size() > 0){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public Station next() {
        int i =0;
        if (!hasNext()){
            stationDistance = null;
        }
        for (Station st : stationsDispo){
            i++;
            if (i == 1){
                distance = stationDepart.distance(st);
                stationDistance = st;
            }
            else{
                if (stationDepart.distance(st) < distance){
                    distance = stationDepart.distance(st);
                    stationDistance = st;
                }
            }
        }
        stationDepart = stationDistance;
        stationsDispo.remove(stationDistance);
        return stationDistance;
    }

    @Override
    public void remove() {

    }

}
