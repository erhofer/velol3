package fr.ufc.l3info.oprog;

abstract public class Option implements IVelo{
    private double prix;
    private IVelo v;
    private String cadre;

    public Option(IVelo velo, double t, String type){
        this.v = velo;
        this.prix = t;
        this.cadre = type;
    }

    @Override
    public double kilometrage() {
        return v.kilometrage();
    }

    @Override
    public double prochaineRevision() {
        return v.prochaineRevision();
    }

    @Override
    public void parcourir(double km) {
        v.parcourir(km);
    }

    @Override
    public double tarif() {
        return v.tarif() + this.prix;
    }

    @Override
    public int decrocher() {
        return v.decrocher();
    }

    @Override
    public int arrimer() {
        return v.arrimer();
    }

    @Override
    public void abimer() {
        v.abimer();
    }

    @Override
    public boolean estAbime() {
        return v.estAbime();
    }

    @Override
    public int reviser() {
        return v.reviser();
    }

    @Override
    public int reparer() {
        return v.reparer();
    }

    @Override
    public String toString() {
        //if (this.v.toString().contains("femme")) {
        //System.out.println(this.cadre);
            return this.v.toString().replace(" - ", ", "+this.cadre+" - ");
        //}
    }
}
