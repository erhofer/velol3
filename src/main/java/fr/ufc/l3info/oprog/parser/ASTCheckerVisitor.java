package fr.ufc.l3info.oprog.parser;

import java.util.HashMap;
import java.util.Map;

/**
 * Visiteur réalisant des vérifications sur l'AST du fichier de stations.
 */
public class ASTCheckerVisitor implements ASTNodeVisitor {

    private Map<String, ERROR_KIND> erreurs;

    public ASTCheckerVisitor() {
        erreurs = new HashMap<>();
    }

    public Map<String, ERROR_KIND> getErrors() {
        return erreurs;    // TODO
    }

    @Override
    public Object visit(ASTNode n) {
        if(n.getNumChildren() == 0){
            erreurs.put("Ce fichier est vide",ERROR_KIND.EMPTY_LIST);
        }
        return null;
    }

    @Override
    public Object visit(ASTListeStations n) {
        String nom1 = n.getChild(0).value;
        String nom2 = n.getChild(0).value;
        int taille = n.getNumChildren();
        for (int i = 0; i < taille ; i++){
            nom1 = n.getChild(i).value;
            for (int j = 0; j < taille ; j++){
                nom2 = n.getChild(j).value;
                if (i != j && nom1.equals(nom2)){
                    erreurs.put("Deux stations ont le même nom",ERROR_KIND.DUPLICATE_STATION_NAME);
                }
            }
        }
        return null;
    }

    @Override
    public Object visit(ASTStation n) {
        String nom = n.getChild(0).value;
        //System.out.println(n.getChild(0).value);
        if (nom.isEmpty()){
            erreurs.put("Cette Station a un nom vide",ERROR_KIND.EMPTY_STATION_NAME);
            return null;
        }
        if (nom.trim().isEmpty()){
            erreurs.put("Cette Station a un nom contenant que des espaces",ERROR_KIND.EMPTY_STATION_NAME);
            return null;
        }
        int nb1 = 0;
        int nb2 = 0;
        int nb3 = 0;
        for (int i = 1; i < n.getNumChildren(); i++){
            if(n.getChild(i).getChild(0).value.equals("capacite")){
                nb1++;
            }
            if(n.getChild(i).getChild(0).value.equals("latitude")){
                nb2++;
            }
            if(n.getChild(i).getChild(0).value.equals("longitude")){
                nb3++;
            }
        }
        if (nb1 < 1 || nb2 < 1 || nb3 < 1){
            erreurs.put("Il manque une déclaration",ERROR_KIND.MISSING_DECLARATION);
        }
        if (nb1 > 1 || nb2 > 1 || nb3 > 1 || n.getNumChildren() > 4){
            erreurs.put("il y a une déclaration en trop",ERROR_KIND.DUPLICATE_DECLARATION);
        }
        return null;
    }

    @Override
    public Object visit(ASTDeclaration n) {

        return null;
    }

    @Override
    public Object visit(ASTChaine n) {
        String nom = n.value;
        if (nom.equals("\"\"")){
            erreurs.put("Cette Station a un nom vide",ERROR_KIND.EMPTY_STATION_NAME);
        }
        if (nom.replace(" ","").equals("\"\"")){
            erreurs.put("Cette Station a un nom contenant que des espaces",ERROR_KIND.EMPTY_STATION_NAME);
        }
        return null;
    }

    @Override
    public Object visit(ASTIdentificateur n) {
        return n.toString();
    }

    @Override
    public Object visit(ASTNombre n){
        if (n.getNumberValue() < 0){
            erreurs.put("le nombre est null",ERROR_KIND.WRONG_NUMBER_VALUE);
        }
        if ((""+n.getNumberValue()).contains(".")){
            erreurs.put("le nombre n'est pas un entier",ERROR_KIND.WRONG_NUMBER_VALUE);
        }
        return null;
    }
}

enum ERROR_KIND {
    EMPTY_LIST,
    EMPTY_STATION_NAME,
    DUPLICATE_STATION_NAME,
    MISSING_DECLARATION,
    DUPLICATE_DECLARATION,
    WRONG_NUMBER_VALUE
}