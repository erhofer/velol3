package fr.ufc.l3info.oprog;

/**
 * Classe représentant un abonné au service VéloCité.
 */
public class Abonne {
    public static int compteurAbonnes = 0;
    private String nom;
    private String rib;
    private boolean bloque;
    private int ID;
    /**
     * Créé un abonné dont le nom est passé en paramètre, sans informations bancaires. 
     *  Si le nom de l'abonné n'est pas correct (vide ou ne contenant pas des lettres éventuellementséparées par des espaces ou des traits d'union), le constructeur déclenchera l'exception IncorrectNameException.
     *  On notera que le nom peut contenir des espaces inutiles en début et en fin, mais ceux-ci seront retirés pour enregistrer cette donnée.
     * @param nom le nom du nouvel abonné.  
     * @throws IncorrectNameException si le nom de l'abonné n'est pas correct.
     */
    public Abonne(String nom) throws IncorrectNameException {
        if(!nom.equals("") && nom.matches("^[\\s]*[a-zA-ZÀ-ÿ]+([-|\\s][a-zA-ZÀ-ÿ]+)*[a-zA-ZÀ-ÿ]*[\\s]*$")) {
            this.nom = nom.trim();
            this.ID=this.compteurAbonnes;
            this.compteurAbonnes = this.compteurAbonnes+1;
            this.bloque=true;
        }else{
            throw new IncorrectNameException();
        }
    }

    /**
     * Créé un abonné dont le nom est passé en paramètre, avec les informations bancaires spécifiées dans le second paramètre.
     *  Le comportement attendu est le même que celui du constructeur précédent. Le RIB n'est enregistré que si celui-ci est valide.
     * @param nom le nom du nouvel abonné.
     * @param rib le RIB
     * @throws IncorrectNameException si le nom de l'abonné n'est pas correct.
     */
    public Abonne(String nom, String rib) throws IncorrectNameException {
        this(nom);

        if (rib.matches("[0-9]{5}[-][0-9]{5}[-][0-9]{11}[-][0-9]{2}")){
            String[] strArray;
            strArray = rib.split("-");
            long cal_rib =  97 - ((89 * Long.parseLong(strArray[0]) + 15 * Long.parseLong(strArray[1]) + 3 * Long.parseLong(strArray[2]) ) %97);
            if (cal_rib != Long.parseLong(strArray[3])){
                this.bloque=true;
                //throw new IncorrectNameException();
            }else {
                this.bloque = false;
                this.rib = rib;

                this.ID=this.compteurAbonnes;
                this.compteurAbonnes = this.compteurAbonnes+1;
            }
        }else {
            this.bloque=true;
            //throw new IncorrectNameException();
        }
    }

    /**
     * Renvoie l'identifiant de l'abonné, généré autoamtiquement à sa création.
     * @return l'identifiant de l'abonné.
     */
    public int getID() {
        return this.ID;
    }

    /**
     * Renvoie le nom de l'abonné.
     * @return le nom de l'abonné, sans les éventuels espace en début et en fin de chaîne.
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Met à jour l'ancien RIB pour un nouveau. Si le nouveau RIB n'est pas valide, l'abonné conserve ses anciennes coordonnées bancaires.
     * @param rib nouveau RIB pour la mise à jour. 
     */
    public void miseAJourRIB(String rib) {
        if (rib.matches("[0-9]{5}[-][0-9]{5}[-][0-9]{11}[-][0-9]{2}")) {
            String[] strArray;
            strArray = rib.split("-");
            long cal_rib = 97 - ((89 * Long.parseLong(strArray[0]) + 15 * Long.parseLong(strArray[1]) + 3 * Long.parseLong(strArray[2])) % 97);
            if(cal_rib == Long.parseLong(strArray[3])){
                this.rib = rib;
                debloquer();
            }
        }
    }

    /**
     * Permet de bloquer volontairement un abonné.
     */
    public void bloquer() {
        this.bloque = true;
    }

    /**
     * Permet de débloquer un abonné. 
     */
    public void debloquer() {
        if(this.rib != null) {
            this.bloque = false;
        }
    }

    /**
     * Vérifie si un abonné est bloqué. Celui-ci peut être bloqué volontairement ou parce que ses coordonnées bancaires sont invalides.
     * @return true si l'abonné est considéré comme bloqué, false sinon.
     */
    public boolean estBloque() {
        return this.bloque;
    }

    /**
     * permet de tester si deux abonnés sont identiques. Pour cela, on vérifiera si leur identifiant est le même.
     * @param a l'abonné avec lequel est comparé l'instance courante. 
     * @return true si les deux objets ont le même ID, false sinon. 
     */
    public boolean equals(Object a) {
        if(!(a instanceof Abonne)){
            return false;
        }
        Abonne other = (Abonne)a;
        return this.getID()==other.getID();
    }

    /**
     * Utilisée en interne par Java pour obtenir un hash de l'objet. Cette méthode est utilisée pour les structures de collection de type HashSet ou HashMap.
     * @return le hash de l'instance courante. 
     */
    public int hashCode() {
        //System.out.println(nom.length());
        return super.hashCode();
    }

}

class IncorrectNameException extends Exception {
    public IncorrectNameException() {
        super("Le nom fourni n'est pas correct.");
    }
}
