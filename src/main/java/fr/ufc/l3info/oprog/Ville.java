package fr.ufc.l3info.oprog;

import fr.ufc.l3info.oprog.parser.ASTNode;
import fr.ufc.l3info.oprog.parser.ASTStationBuilder;
import fr.ufc.l3info.oprog.parser.StationParser;
import fr.ufc.l3info.oprog.parser.StationParserException;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Ville implements Iterable<Station> {
    final StationParser parser = StationParser.getInstance();
    private Station stationPrincipal;
    private Set<Station> maVille;
    private double distance;
    private ClosestStationIterator closestStationIterator;

    public Ville() {
        maVille = new HashSet<>();
    }

    public void initialiser(File f) throws IOException, StationParserException {
        ASTNode n = parser.parse(f);
        ASTStationBuilder builder = new ASTStationBuilder();
        n.accept(builder);
        int i = 0;
        for (Station s : builder.getStations()) {
            if (i == 0) {
                this.stationPrincipal = s;
            }
            i++;
        }
    }

    public void setStationPrincipal(Station st) {
        this.stationPrincipal = st;
    }

    public Station getStation(String nom) {
        for (Station s : maVille){
            if (s.getNom() == nom){
                return s;
            }
        }
        return null;
    }

    public Station getStationPlusProche(double lat, double lon){
        int i = 0;
        Station stationDistance = null;
        Station stationDepart = new Station("", lat, lon, 10);
        for (Station st : maVille){
            i++;
            if (i == 1){
                distance = stationDepart.distance(st);
                stationDistance = st;
            }
            else{
                if (stationDepart.distance(st) < distance){
                    distance = stationDepart.distance(st);
                    stationDistance = st;
                }
            }
        }
        return stationDistance;
    }

    public Abonne creerAbonne(String nom, String RIB){
        try {
            return new Abonne(nom, RIB);
        } catch (IncorrectNameException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterator<Station> iterator() {
        closestStationIterator = new ClosestStationIterator(maVille, stationPrincipal);
        return closestStationIterator;
    }

    public Map<Abonne, Double> facturation(int mois, int annee){
        return null;
    }
}
