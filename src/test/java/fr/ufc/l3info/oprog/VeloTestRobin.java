package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

public class VeloTestRobin {

    @Test
    public void testVeloInitSimple(){
        Velo a = new Velo();

        Assert.assertEquals("Vélo cadre mixte - 0.0 km", a.toString());
    }

    @Test
    public void testVeloInitHomme(){
        Velo a = new Velo('H');
        Velo b = new Velo('h');

        Assert.assertEquals("Vélo cadre homme - 0.0 km", a.toString());
        Assert.assertEquals("Vélo cadre homme - 0.0 km", b.toString());
    }

    @Test
    public void testVeloInitFemme(){
        Velo a = new Velo('F');
        Velo b = new Velo('f');

        Assert.assertEquals("Vélo cadre femme - 0.0 km", a.toString());
        Assert.assertEquals("Vélo cadre femme - 0.0 km", b.toString());
    }

    @Test
    public void testVeloInitMixte(){
        Velo a = new Velo('z');

        Assert.assertEquals("Vélo cadre mixte - 0.0 km", a.toString());
    }

    @Test
    public void testVeloInitCharVide(){
        Velo a = new Velo(' ');

        Assert.assertEquals("Vélo cadre mixte - 0.0 km", a.toString());
    }

    //------------------------------------------------------------------------

    @Test
    public void testKilometrageInit(){
        Velo a = new Velo();

        Assert.assertEquals(0, a.kilometrage(), 0);
    }

    @Test
    public void testTarifInit(){
        Velo a = new Velo();

        Assert.assertEquals(2.0, a.tarif(), 0);
    }

    @Test
    public void testEstAbimerInit(){
        Velo a = new Velo();

        Assert.assertFalse(a.estAbime());
    }

    @Test
    public void testRevisionInit(){
        Velo a = new Velo();

        Assert.assertEquals(500, a.prochaineRevision(),0);
    }

    //--------------------------------------------------------------------------

    @Test
    public void testDecrocherInit(){
        Velo a = new Velo();

        Assert.assertEquals(-1, a.decrocher());
    }

    @Test
    public void testDecrocherVeloDecroche(){
        Velo a = new Velo();

        a.arrimer();
        a.decrocher();

        Assert.assertEquals(-1, a.decrocher());
    }

    @Test
    public void testDecrocherVeloAcroche(){
        Velo a = new Velo();

        a.arrimer();

        Assert.assertEquals(0, a.decrocher());
    }

    //-----------------------------------------------------------

    @Test
    public void testArrimerInit(){
        Velo a = new Velo();

        Assert.assertEquals(0, a.arrimer());
    }

    @Test
    public void testArrimerVeloDecroche(){
        Velo a = new Velo();

        a.arrimer();
        a.decrocher();

        Assert.assertEquals(0, a.arrimer());
    }

    @Test
    public void testArrimerVeloAcroche(){
        Velo a = new Velo();

        a.arrimer();

        Assert.assertEquals(-1, a.arrimer());
    }

    //--------------------------------------------------------

    @Test
    public void testAbimerVeloNeuf(){
        Velo a = new Velo();

        a.abimer();

        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testAbimerVeloAbimer(){
        Velo a = new Velo();

        a.abimer();
        a.abimer();

        Assert.assertTrue(a.estAbime());
    }

    //----------------------------------------------

    @Test
    public void testReparerVeloNeufDecroche(){
        Velo a = new Velo();

        a.decrocher();

        Assert.assertEquals(-2, a.reparer());
    }

    @Test
    public void testReparerVeloNeufAccroche(){
        Velo a = new Velo();

        a.arrimer();

        Assert.assertEquals(-1, a.reparer());
    }

    @Test
    public void testReparerVeloAbimerDecroche(){
        Velo a = new Velo();

        a.decrocher();
        a.abimer();

        Assert.assertEquals(0, a.reparer());
    }

    @Test
    public void testReparerVeloAbimerAccroche(){
        Velo a = new Velo();

        a.abimer();
        a.arrimer();

        Assert.assertEquals(-1, a.reparer());
    }

    //----------------------------------------------------

    @Test
    public void testParcourirDecrocher(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(18.98);

        Assert.assertEquals(18.98, a.kilometrage(),0);
    }

    @Test
    public void testParcourirNegatifDecrocher(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(-18.98);

        Assert.assertEquals(0, a.kilometrage(),0);
    }

    @Test
    public void testParcourirAccrocher(){
        Velo a = new Velo();

        a.arrimer();
        a.parcourir(1.5);

        Assert.assertEquals(0, a.kilometrage(),0);
    }

    @Test
    public void testParcourirNegatifAccrocher(){
        Velo a = new Velo();

        a.arrimer();
        a.parcourir(-65.15);

        Assert.assertEquals(0, a.kilometrage(),0);
    }

    //----------------------------------------------------------

    @Test
    public void testRevisionApresParcours(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(59);

        Assert.assertEquals(441, a.prochaineRevision(), 0);
    }

    @Test
    public void testRevisionApresParcoursGrand(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(590);

        Assert.assertEquals(-90, a.prochaineRevision(), 0);
    }

    //----------------------------------------------------------

    @Test
    public void testToStringKilometrageArrondieSuperieur(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(16.46);

        Assert.assertEquals("Vélo cadre mixte - 16.5 km", a.toString());
    }

    @Test
    public void testToStringKilometrageArrondieInferieur(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(16.44);

        Assert.assertEquals("Vélo cadre mixte - 16.4 km", a.toString());
    }

    @Test
    public void testToStringKilometrage(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(16.6);

        Assert.assertEquals("Vélo cadre mixte - 16.6 km", a.toString());
    }

    @Test
    public void testToStringKilometrageTropGrand(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(500);

        Assert.assertEquals("Vélo cadre mixte - 500.0 km (révision nécessaire)", a.toString());
    }

    //----------------------------------------------------

    @Test
    public void testReviserAccrocher(){
        Velo a = new Velo();

        a.arrimer();

        Assert.assertEquals(-1, a.reviser());
    }

    @Test
    public void testReviserDecrocherPasAbimer(){
        Velo a = new Velo();

        a.decrocher();

        Assert.assertEquals(0, a.reviser());
    }

    @Test
    public void testReviserDecrocherAbimer(){
        Velo a = new Velo();

        a.decrocher();
        a.abimer();
        a.reviser();

        Assert.assertFalse(a.estAbime());
    }

    @Test
    public void testReviserAccrocherAbimer(){
        Velo a = new Velo();

        a.abimer();
        a.arrimer();
        a.reviser();

        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testReviserDecrocherKilometrage(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(125.65);
        a.reviser();

        Assert.assertEquals(0, a.kilometrage(), 0);
    }

    @Test
    public void testReviserAccrocherKilometrage(){
        Velo a = new Velo();

        a.decrocher();
        a.parcourir(125.65);
        a.arrimer();
        a.reviser();

        Assert.assertEquals(125.65, a.kilometrage(), 0);
    }
}
