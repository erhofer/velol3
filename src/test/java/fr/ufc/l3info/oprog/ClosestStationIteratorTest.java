package fr.ufc.l3info.oprog;

import fr.ufc.l3info.oprog.parser.ASTNode;
import fr.ufc.l3info.oprog.parser.ASTStationBuilder;
import fr.ufc.l3info.oprog.parser.StationParser;
import fr.ufc.l3info.oprog.parser.StationParserException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ClosestStationIteratorTest {

    /** Chemin vers les fichiers de test */
    final String path = "./target/classes/data/";

    /** Instance singleton du parser de stations */
    final StationParser parser = StationParser.getInstance();

    @Test
    public void testHasNextTrue() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationsOK.txt"));
        ASTStationBuilder builder = new ASTStationBuilder();
        n.accept(builder);
        ClosestStationIterator closestStationIterator = new ClosestStationIterator(builder.getStations(), builder.getStations().iterator().next());
        assertTrue(closestStationIterator.hasNext());
    }

    @Test
    public void testHasNextFalse() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationsOK.txt"));
        ASTStationBuilder builder = new ASTStationBuilder();
        n.accept(builder);
        ClosestStationIterator closestStationIterator = new ClosestStationIterator(builder.getStations(), builder.getStations().iterator().next());
        closestStationIterator.next();
        closestStationIterator.next();
        closestStationIterator.next();
        closestStationIterator.next();
        closestStationIterator.next();
        closestStationIterator.next();
        closestStationIterator.next();
        assertFalse(closestStationIterator.hasNext());
    }

    @Test
    public void testHasNextTrueMemeAvecNext() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationsOK.txt"));
        ASTStationBuilder builder = new ASTStationBuilder();
        n.accept(builder);
        ClosestStationIterator closestStationIterator = new ClosestStationIterator(builder.getStations(), builder.getStations().iterator().next());
        assertTrue(closestStationIterator.hasNext());
    }

    @Test
    public void testNext() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationsOK.txt"));
        ASTStationBuilder builder = new ASTStationBuilder();
        n.accept(builder);
        ClosestStationIterator closestStationIterator = new ClosestStationIterator(builder.getStations(), builder.getStations().iterator().next());
        closestStationIterator.next();
        assertNull(closestStationIterator.next());
    }

    @Test
    public void testNextBonneStation() throws IOException, StationParserException {
        Station station1 = new Station("station1", 0.0, 1.0, 10);
        Station station2 = new Station("station2", 1.0, 1.0, 10);
        Station station3 = new Station("station3", 5.0, 1.0, 10);
        Station station4 = new Station("station4", 3.0, 1.0, 10);
        Station station5 = new Station("station5", 4.0, 1.0, 10);
        Set<Station> stationSet = new HashSet<>();
        stationSet.add(station1);
        stationSet.add(station2);
        stationSet.add(station3);
        stationSet.add(station4);
        stationSet.add(station5);

        ClosestStationIterator closestStationIterator = new ClosestStationIterator(stationSet, station1);
        closestStationIterator.next();
        closestStationIterator.next();
        assertEquals(station5, closestStationIterator.next());
    }

    @Test
    public void testNextBonneStation2() throws IOException, StationParserException {
        Station station1 = new Station("station1", 0.0, 1.0, 10);
        Station station2 = new Station("station2", 1.0, 1.0, 10);
        Station station3 = new Station("station3", 2.0, 1.0, 10);
        Station station4 = new Station("station4", 3.0, 1.0, 10);
        Station station5 = new Station("station5", 4.0, 1.0, 10);
        Set<Station> stationSet = new HashSet<>();
        stationSet.add(station1);
        stationSet.add(station2);
        stationSet.add(station3);
        stationSet.add(station4);
        stationSet.add(station5);

        ClosestStationIterator closestStationIterator = new ClosestStationIterator(stationSet, station1);
        closestStationIterator.next();
        closestStationIterator.next();
        assertEquals(station4, closestStationIterator.next());
    }

    @Test
    public void testNextMauvaiseStation() throws IOException, StationParserException {
        Station station1 = new Station("station1", 0.0, 1.0, 10);
        Station station2 = new Station("station2", 1.0, 1.0, 10);
        Station station3 = new Station("station3", 5.0, 1.0, 10);
        Station station4 = new Station("station4", 3.0, 1.0, 10);
        Station station5 = new Station("station5", 4.0, 1.0, 10);
        Set<Station> stationSet = new HashSet<>();
        stationSet.add(station1);
        stationSet.add(station2);
        stationSet.add(station3);
        stationSet.add(station4);
        stationSet.add(station5);

        ClosestStationIterator closestStationIterator = new ClosestStationIterator(stationSet, station1);
        closestStationIterator.next();
        closestStationIterator.next();
        assertNotEquals(station3, closestStationIterator.next());
    }

}
