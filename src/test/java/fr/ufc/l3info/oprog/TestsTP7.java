package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

public class TestsTP7 {

    @Test
    public void testEmprunterAbonne() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,15);
        Assert.assertEquals(a,r.emprunter(v));
    }

    @Test
    public void testEmprunterAbonneBloque() throws IncorrectNameException {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Assert.assertNull(r.emprunter(v));
    }

    @Test
    public void testEmprunterMaisVeloCasser() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        v.abimer();
        Assert.assertEquals(-2, r.emprunter(a,v,15));
    }

    @Test
    public void testRetournerVeloCasserAbonnePrison() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,15);
        v.abimer();
        r.retourner(v, 15);
        Assert.assertTrue(a.estBloque());
    }

    /*---*/

    @Test
    public void testTarifNormal() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,0);
        r.retourner(v, 3600000);
        Assert.assertEquals( 2.00,r.facturation(a,0,7200000),0.001);
    }

    @Test
    public void testTarif50pourcent() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        for (int i = 0; i < 51; i++) {
            r.emprunter(a,v,0);
            r.retourner(v, 3600000);
        }
        Assert.assertEquals( 51.0,r.facturation(a,0,12000000),0.001);
    }

    @Test
    public void testTarif40pourcent() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        for (int i = 0; i < 45; i++) {
            r.emprunter(a,v,0);
            r.retourner(v, 3600000);
        }
        Assert.assertEquals( 54.0,r.facturation(a,0,12000000),0.001);
    }

    @Test
    public void testTarif30pourcent() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        for (int i = 0; i < 35; i++) {
            r.emprunter(a,v,0);
            r.retourner(v, 3600000);
        }
        Assert.assertEquals( 49.0,r.facturation(a,0,12000000),0.001);
    }

    @Test
    public void testTarif20pourcent() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        for (int i = 0; i < 25; i++) {
            r.emprunter(a,v,0);
            r.retourner(v, 3600000);
        }
        Assert.assertEquals( 40.0,r.facturation(a,0,12000000),0.001);
    }

    @Test
    public void testTarif10pourcent() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        for (int i = 0; i < 15; i++) {
            r.emprunter(a,v,0);
            r.retourner(v, 3600000);
        }
        Assert.assertEquals( 27.0,r.facturation(a,0,12000000),0.001);
    }

    @Test
    public void testTarifPourAutreAbonne() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        Abonne b = new Abonne("Fredo", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
            r.emprunter(a,v,0);
            r.retourner(v, 3600000);

        Assert.assertEquals( 0.0,r.facturation(b,0,12000000),0.001);
    }

}
