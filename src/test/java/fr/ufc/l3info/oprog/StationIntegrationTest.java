package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;


public class StationIntegrationTest {
    @Test
    public void testEmpruntAbonneBloque() throws IncorrectNameException {
        // instanciation du mock
        Abonne a = new Abonne("Fred");
        IRegistre r = new JRegistre();
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(r);
        Assert.assertNull(s.emprunterVelo(a, 1));
    }

    @Test
    public void testEmpruntRegNull() throws IncorrectNameException {
        // instanciation du mock
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertNull(s.emprunterVelo(a, 1));
    }

    @Test
    public void testEmpruntEnCoursNull() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        IRegistre r = new JRegistre();
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(r);
        Assert.assertNull(s.emprunterVelo(a, 1));
    }

    @Test
    public void testEmpruntOk() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        // objet sous test
        Station s = new Station("ok",0,0,10);
        r.emprunter(a,v,1);
        s.setRegistre(r);
        s.arrimerVelo(v, 1);
        Assert.assertEquals(v,s.emprunterVelo(a, 1));
    }

    @Test
    public void testArrimerInf1() {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertEquals(-1,s.arrimerVelo(v, 0));
    }

    @Test
    public void testArrimerSupCap() {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertEquals(-1,s.arrimerVelo(v, 11));
    }

    @Test
    public void testArrimerVeloNull() {
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertEquals(-1,s.arrimerVelo(null, 1));
    }

    @Test
    public void testArrimerRegRetourner() {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(r);
        Assert.assertEquals(-4,s.arrimerVelo(v, 1));
    }

    @Test
    public void testArrimerRegNull() {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        //IRegistre r = new JRegistre();
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(null);
        Assert.assertEquals(-2,s.arrimerVelo(v, 1));
    }

    /*@Test
    public void testArrimerOk() throws IncorrectNameException {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        // objet sous test
        Station s = new Station("ok",0,0,10);
        //r.emprunter(a,v,1);
        s.setRegistre(r);
        Assert.assertEquals(0,s.arrimerVelo(v, 1));
    }*/

    @Test
    public void testArrimerDistance() {
        Station s = new Station("ok",10.0,20.0,10);
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertEquals(s1.distance(s),s.distance(s1),0.001);
    }

    @Test
    public void testArrimerDistanceSame() {
        Station s = new Station("ok",10.0,20.0,10);
        Station s1 = new Station("ok",10.0,20.0,10);
        Assert.assertEquals(0,s.distance(s1),0.001);
    }

    @Test
    public void testArrimerDistance100() {
        Station s = new Station("ok",10.0,20.0,10);
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertEquals(1111.9,s.distance(s1),0.1);
    }

    @Test
    public void testNom() {
        String ch = "ok";
        Station s1 = new Station(ch,0.0,20.0,10);
        Assert.assertEquals(ch, s1.getNom());
    }

    @Test
    public void testALaBorneSup() {
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertNull(s1.veloALaBorne(11));
    }

    @Test
    public void testALaBorneInf() {
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertNull(s1.veloALaBorne(0));
    }

    @Test
    public void testBorneLibre10() {
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertEquals(10,s1.nbBornesLibres());
    }

    @Test
    public void testBorneLibreZero() {
        Station s1 = new Station("ok",0.0,20.0,0);
        Assert.assertEquals(0,s1.nbBornesLibres());
    }

    @Test
    public void testBorneLibreNotNull() throws IncorrectNameException {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        // objet sous test
        Station s = new Station("ok",0,0,10);
        r.emprunter(a,v,1);
        s.setRegistre(r);
        s.arrimerVelo(v, 1);
        Assert.assertEquals(9,s.nbBornesLibres());
    }

    @Test
    public void testArrimerBPris() throws IncorrectNameException {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v1 = fab.construire('H',"");
        IVelo v2 = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        // objet sous test
        Station s = new Station("ok",0,0,10);
        r.emprunter(a,v1,1);
        r.emprunter(a,v2,1);
        s.setRegistre(r);
        s.arrimerVelo(v2, 1);
        Assert.assertEquals(-2,s.arrimerVelo(v1, 1));
    }

    @Test
    public void testEquilibrerSetNullBefore() throws IncorrectNameException {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        Station s = new Station("ok",0,0,10);
        // objet sous test
        s.setRegistre(r);
        Set<IVelo> velos = new HashSet<>();
        s.equilibrer(velos);
        r.emprunter(a,v,1);
        Assert.assertEquals(0,s.arrimerVelo(v, 1));

    }
}