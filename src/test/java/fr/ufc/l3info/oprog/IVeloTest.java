package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

public class IVeloTest {
    private FabriqueVelo fab;

    @Test
    public void testEmpty(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"");
        Assert.assertTrue(a.toString().contains("Vélo cadre homme - 0.0 km"));
    }
    @Test
    public void testCadreAluS(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertTrue(a.toString().contains("cadre aluminium"));
    }
    @Test
    public void testOptFreinsDisqueS(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE");
        Assert.assertTrue(a.toString().contains("freins à disque"));
    }
    @Test
    public void testOptSuspensionAvantS(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT");
        Assert.assertTrue(a.toString().contains("suspension avant"));
    }
    @Test
    public void testOptSuspensionArriereS(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("suspension arrière"));
    }
    @Test
    public void testOptAssistanceElectriqueS(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains(" assistance électrique"));
    }
    @Test
    public void testNoOptS(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"RETROVISEUR");
        Assert.assertTrue(a.toString().contains("homme -"));
    }
    //----------------
    @Test
    public void testCadreAluT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertEquals(2.2,a.tarif(),0.01);
    }
    @Test
    public void testOptFreinsDisqueT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE");
        Assert.assertEquals(2.3,a.tarif(),0.01);
    }
    @Test
    public void testOptSuspensionAvantT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT");
        Assert.assertEquals(2.5,a.tarif(),0.01);
    }
    @Test
    public void testOptSuspensionArriereT() {
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_ARRIERE");
        Assert.assertEquals(2.5,a.tarif(),0.01);
    }
    @Test
    public void testOptAssistanceElectriqueT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4,a.tarif(),0.01);
    }
    @Test
    public void testNoOptT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"RETROVIEUR");
        Assert.assertEquals(2.0,a.tarif(),0.01);
    }
    //----------------
    @Test
    public void testTwoAlu(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","CADRE_ALUMINIUM");
        Assert.assertEquals(2.2,a.tarif(),0.01);
    }
    @Test
    public void testYwoFreins(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","FREINS_DISQUE");
        Assert.assertEquals(2.3,a.tarif(),0.01);
    }
    @Test
    public void testTwoAvant(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","SUSPENSION_AVANT");
        Assert.assertEquals(2.5,a.tarif(),0.01);
    }
    @Test
    public void testTwoArriere() {
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_ARRIERE","SUSPENSION_ARRIERE");
        Assert.assertEquals(2.5,a.tarif(),0.01);
    }
    @Test
    public void testTwoElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"ASSISTANCE_ELECTRIQUE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4,a.tarif(),0.01);
    }
    //------------
    @Test
    public void testAluFreins(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE");
        Assert.assertEquals(2.5,a.tarif(),0.01);
    }
    @Test
    public void testAluAvant(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT");
        Assert.assertEquals(2.7,a.tarif(),0.01);
    }
    @Test
    public void testAluArriere(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_ARRIERE");
        Assert.assertEquals(2.7,a.tarif(),0.01);
    }
    @Test
    public void testAluElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.2,a.tarif(),0.01);
    }
    //--------------
    @Test
    public void testFreinsAvant(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT");
        Assert.assertEquals(2.8,a.tarif(),0.01);
    }
    @Test
    public void testFreinsArriere(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_ARRIERE");
        Assert.assertEquals(2.8,a.tarif(),0.01);
    }
    @Test
    public void testFreinsElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.3,a.tarif(),0.01);
    }
    //------------
    @Test
    public void testAvantArriere(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        Assert.assertEquals(3,a.tarif(),0.01);
    }
    @Test
    public void testAvantElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.5,a.tarif(),0.01);
    }
    //------------
    @Test
    public void testArriereElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.5,a.tarif(),0.01);
    }

    //----------

    @Test
    public void testAluFreinsAvantT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT");
        Assert.assertEquals(3,a.tarif(),0.01);
    }
    @Test
    public void testAluFreinsArriereT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_ARRIERE");
        Assert.assertEquals(3,a.tarif(),0.01);
    }
    @Test
    public void testAluFreinsElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.5,a.tarif(),0.01);
    }
    @Test
    public void testAluAvantArriereT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        //System.out.println(a.toString());
        Assert.assertEquals(3.2,a.tarif(),0.01);
    }
    @Test
    public void testAluAvantElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.7,a.tarif(),0.01);
    }
    @Test
    public void testAluArriereElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.8,a.tarif(),0.01);
    }
    @Test
    public void testFreinsAvantArriereT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        Assert.assertEquals(3.3,a.tarif(),0.01);
    }
    @Test
    public void testFreinsAvantElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.8,a.tarif(),0.01);
    }
    @Test
    public void testFreinsArriereElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(4.8,a.tarif(),0.01);
    }
    @Test
    public void testAvantArriereElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals( 5,a.tarif(),0.01);
    }

    //----------
    //----------

    @Test
    public void testAluFreinsAvantArriereT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        Assert.assertEquals(3.5,a.tarif(),0.01);
    }
    @Test
    public void testAluFreinsAvantElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(5,a.tarif(),0.01);
    }
    @Test
    public void testAluFreinsArriereElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(5,a.tarif(),0.01);
    }
    @Test
    public void testAluAvantArriereElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(5.2,a.tarif(),0.01);
    }
    @Test
    public void testFreinsAvantArriereElecT(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(5.3,a.tarif(),0.01);
    }
    //----------

    @Test
    public void testAllTarif(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals(5.5,a.tarif(),0.01);
    }

    //----------
    //----------

    @Test
    public void testTwoAluChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","CADRE_ALUMINIUM");
        Assert.assertTrue(a.toString().contains("cadre aluminium"));
    }
    @Test
    public void testYwoFreinsChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","FREINS_DISQUE");
        Assert.assertTrue(a.toString().contains("freins à disque"));
    }
    @Test
    public void testTwoAvantChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","SUSPENSION_AVANT");
        Assert.assertTrue(a.toString().contains("suspension avant"));
    }
    @Test
    public void testTwoArriereChaine() {
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_ARRIERE","SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("suspension arrière"));
    }
    @Test
    public void testTwoElecChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"ASSISTANCE_ELECTRIQUE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("assistance électrique"));
    }
    //------------
    @Test
    public void testAluFreinsChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE");
        //System.out.println(a.toString());
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque"));
    }
    @Test
    public void testAluAvantChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT");
        Assert.assertTrue(a.toString().contains("cadre aluminium, suspension avant"));
    }
    @Test
    public void testAluArriereChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, suspension arrière"));
    }
    @Test
    public void testAluElecChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, assistance électrique"));
    }
    //--------------
    @Test
    public void testFreinsAvantChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT");
        Assert.assertTrue(a.toString().contains("freins à disque, suspension avant"));
    }
    @Test
    public void testFreinsArriereChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("freins à disque, suspension arrière"));
    }
    @Test
    public void testFreinsElecChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("freins à disque, assistance électrique"));
    }
    //------------
    @Test
    public void testAvantArriereChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("suspension avant, suspension arrière"));
    }
    @Test
    public void testAvantElecChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("suspension avant, assistance électrique"));
    }
    //------------
    @Test
    public void testArriereElecChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("suspension arrière, assistance électrique"));
    }

    //----------
    //----------

    @Test
    public void testAluFreinsAvant(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, suspension avant"));
    }
    @Test
    public void testAluFreinsArriere(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, suspension arrière"));
    }
    @Test
    public void testAluFreinsElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, assistance électrique"));
    }
    @Test
    public void testAluAvantArriere(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        //System.out.println(a.toString());
        Assert.assertTrue(a.toString().contains("cadre aluminium, suspension avant, suspension arrière"));
    }
    @Test
    public void testAluAvantElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, suspension avant, assistance électrique"));
    }
    @Test
    public void testAluArriereElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("freins à disque, suspension arrière, assistance électrique"));
    }
    @Test
    public void testFreinsAvantArriere(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("freins à disque, suspension avant, suspension arrière"));
    }
    @Test
    public void testFreinsAvantElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("freins à disque, suspension avant, assistance électrique"));
    }
    @Test
    public void testFreinsArriereElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("freins à disque, suspension arrière, assistance électrique"));
    }
    @Test
    public void testAvantArriereElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("suspension avant, suspension arrière, assistance électrique"));
    }

    //----------
    //----------

    @Test
    public void testAluFreinsAvantArriere(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, suspension avant, suspension arrière"));
    }
    @Test
    public void testAluFreinsAvantElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, suspension avant, assistance électrique"));
    }
    @Test
    public void testAluFreinsArriereElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, suspension arrière, assistance électrique"));
    }
    @Test
    public void testAluAvantArriereElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, suspension avant, suspension arrière, assistance électrique"));
    }
    @Test
    public void testFreinsAvantArriereElec(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("freins à disque, suspension avant, suspension arrière, assistance électrique"));
    }
    @Test
    public void testAllChaine(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, suspension avant, suspension arrière, assistance électrique"));
    }
    @Test
    public void testAllChaineWrong(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM","RETROVISEUR","FREINS_DISQUE","SUSPENSION_AVANT","SUSPENSION_ARRIERE","ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(a.toString().contains("cadre aluminium, freins à disque, suspension avant, suspension arrière, assistance électrique"));
    }

    //-----------------
    //-----------------

    @Test
    public void testKM(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertEquals(a.kilometrage(),0,0);
    }
    @Test
    public void testprochaineRevision(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double km = 200.0;
        a.parcourir(km);
        Assert.assertEquals(a.prochaineRevision(),300.0,0.1);
    }
    @Test
    public void testReviserprochaineRevision(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double km2 = 200.0;
        double km1 = 108.0;
        a.parcourir(km1);
        a.reviser();
        a.parcourir(km2);
        Assert.assertEquals(a.prochaineRevision(),300.0,0.1);
    }

    @Test
    public void testDejaDecroche(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.decrocher();
        Assert.assertEquals(a.decrocher(),-1);
    }

    @Test
    public void testDejaArrimer(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.arrimer();
        Assert.assertEquals(a.arrimer(),-1);
    }

    @Test
    public void testArrimer(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertEquals(a.arrimer(),0);
    }

    @Test
    public void testAbime(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.abimer();
        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testtoString(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertNotEquals(a.toString(),"");
    }

    @Test
    public void testParcours(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double km = 200.0;
        a.parcourir(km);
        Assert.assertEquals(a.kilometrage(),km,0.1);
    }

    @Test
    public void testParcoursNeg(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double base = a.kilometrage();
        double km = -200.0;
        a.parcourir(km);
        Assert.assertEquals(a.kilometrage(),base,0.1);
    }

    @Test
    public void testParcoursArrimer(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double base = a.kilometrage();
        a.arrimer();
        double km = 200.0;
        a.parcourir(km);
        Assert.assertEquals(base,a.kilometrage(),0.1);
    }

    @Test
    public void testRemiseAZero(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double km = 500.0;
        a.parcourir(km);
        a.reviser();
        Assert.assertEquals(a.kilometrage(),500,0.1);
    }

    @Test
    public void testRemiseNeg(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double km = 600.0;
        a.parcourir(km);
        Assert.assertEquals(a.prochaineRevision(),-100,0.1);
    }

    @Test
    public void testDecrocher(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.arrimer();
        Assert.assertEquals(a.decrocher(),0);
    }

    @Test
    public void testDejaDecrocher(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertEquals(a.decrocher(),-1);
    }

    @Test
    public void testReparer0(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.abimer();
        Assert.assertEquals(a.reparer(),0);
    }

    @Test
    public void testReparer1(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.arrimer();
        Assert.assertEquals(a.reparer(),-1);
    }

    @Test
    public void testReparer2(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.decrocher();
        Assert.assertEquals(a.reparer(),-2);
    }

    @Test
    public void testReviser1(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.arrimer();
        Assert.assertEquals(a.reviser(),-1);
    }

    @Test
    public void testReviser0(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertEquals(a.reviser(),0);
    }

    @Test
    public void testReviser0Abime(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.abimer();
        Assert.assertEquals(a.reviser(),0);
    }

    @Test
    public void testAbimer(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.abimer();
        Assert.assertEquals(a.reparer(),0);
    }

    @Test
    public void testEstAbimer(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.abimer();
        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testStringf(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('f',"CADRE_ALUMINIUM");
        Assert.assertTrue(a.toString().contains("femme"));
    }

    @Test
    public void testStringfMaj(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('F',"CADRE_ALUMINIUM");
        Assert.assertTrue(a.toString().contains("femme"));
    }

    @Test
    public void testStringh(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('h',"CADRE_ALUMINIUM");
        Assert.assertTrue(a.toString().contains("homme"));
    }

    @Test
    public void testStringhMaj(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        Assert.assertTrue(a.toString().contains("homme"));
    }

    @Test
    public void testStringm(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('a',"CADRE_ALUMINIUM");
        Assert.assertTrue(a.toString().contains("mixte"));
    }

    @Test
    public void testStringRevision(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double km = 600.0;
        a.parcourir(km);
        Assert.assertTrue(a.toString().contains("(révision nécessaire)"));
    }

    @Test
    public void testString1(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        double km = 600.0;
        a.parcourir(km);
        Assert.assertEquals(a.toString(),"Vélo cadre homme, cadre aluminium - 600.0 km (révision nécessaire)");
    }

    @Test
    public void testString2(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('f',"CADRE_ALUMINIUM");
        double km = 600.0;
        a.parcourir(km);
        Assert.assertEquals(a.toString(),"Vélo cadre femme, cadre aluminium - 600.0 km (révision nécessaire)");
    }

    @Test
    public void testString3(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('4',"CADRE_ALUMINIUM");
        double km = 400.0;
        a.parcourir(km);
        Assert.assertEquals(a.toString(),"Vélo cadre mixte, cadre aluminium - 400.0 km");
    }

    @Test
    public void testVeloNoOpt(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('t',"CADRE_ALUMINIUM");
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 0.0 km", a.toString());
    }

    @Test
    public void testToString500(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('t',"CADRE_ALUMINIUM");
        a.decrocher();
        a.parcourir(500);
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 500.0 km (révision nécessaire)", a.toString());
    }

    @Test
    public void testToStringArrondieSup(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('g',"CADRE_ALUMINIUM");
        a.parcourir(15.16);
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 15.2 km", a.toString());
    }

    @Test
    public void testToStringArrondieInf(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('g',"CADRE_ALUMINIUM");
        a.parcourir(15.22);
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 15.2 km", a.toString());
    }

    @Test
    public void testToStringArrondie(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('j',"CADRE_ALUMINIUM");
        a.parcourir(48.15162342);
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 48.2 km", a.toString());
    }

    @Test
    public void testToStringArrondie500(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('j',"CADRE_ALUMINIUM");
        a.parcourir(499.99);
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 500.0 km", a.toString());
    }

    @Test
    public void testAbimerTwoVelo(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.abimer();
        a.abimer();
        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testReviserAccrocherKilometrage(){

        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.decrocher();
        a.parcourir(108);
        a.arrimer();
        a.reviser();
        Assert.assertEquals(108, a.kilometrage(), 0);
    }

    @Test
    public void testReviserKilometrageAccrocher(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.decrocher();
        a.arrimer();
        a.parcourir(108);
        a.reviser();
        Assert.assertEquals(0, a.kilometrage(), 0);
    }
    @Test
    public void testToStringKMRevision(){
        fab = FabriqueVelo.getInstance();
        IVelo a = fab.construire('H',"CADRE_ALUMINIUM");
        a.parcourir(700);
        a.reviser();
        a.parcourir(200);
        Assert.assertEquals("Vélo cadre homme, cadre aluminium - 900.0 km", a.toString());
    }
}
