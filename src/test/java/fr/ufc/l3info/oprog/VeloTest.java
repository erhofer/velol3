package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

public class VeloTest {

    @Test
    public void testKM(){
        Velo a = new Velo();
        Assert.assertEquals(a.kilometrage(),0,0);
    }

    @Test
    public void testprochaineRevision(){
        Velo a = new Velo();
        double km = 200.0;
        a.parcourir(km);
        Assert.assertEquals(a.prochaineRevision(),300.0,0.1);
    }

    @Test
    public void testReviserprochaineRevision(){
        Velo a = new Velo();
        double km2 = 200.0;
        double km1 = 108.0;
        a.parcourir(km1);
        a.reviser();
        a.parcourir(km2);
        Assert.assertEquals(a.prochaineRevision(),300.0,0.1);
    }

    @Test
    public void testDejaDecroche(){
        Velo a = new Velo();
        a.decrocher();
        Assert.assertEquals(a.decrocher(),-1);
    }

    @Test
    public void testDejaArrimer(){
        Velo a = new Velo();
        a.arrimer();
        Assert.assertEquals(a.arrimer(),-1);
    }

    @Test
    public void testArrimer(){
        Velo a = new Velo();
        Assert.assertEquals(a.arrimer(),0);
    }

    @Test
    public void testTarif(){
        Velo a = new Velo();
        Assert.assertEquals(a.tarif(),2,0.1);
    }

    @Test
    public void testAbime(){
        Velo a = new Velo();
        a.abimer();
        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testtoString(){
        Velo a = new Velo();
        Assert.assertNotEquals(a.toString(),"");
    }

    @Test
    public void testParcours(){
        Velo a = new Velo();
        double km = 200.0;
        a.parcourir(km);
        Assert.assertEquals(a.kilometrage(),km,0.1);
    }

    @Test
    public void testParcoursNeg(){
        Velo a = new Velo();
        double base = a.kilometrage();
        double km = -200.0;
        a.parcourir(km);
        Assert.assertEquals(a.kilometrage(),base,0.1);
    }

    @Test
    public void testParcoursArrimer(){
        Velo a = new Velo();
        double base = a.kilometrage();
        a.arrimer();
        double km = 200.0;
        a.parcourir(km);
        Assert.assertEquals(base,a.kilometrage(),0.1);
    }

    @Test
    public void testRemiseAZero(){
        Velo a = new Velo();
        double km = 500.0;
        a.parcourir(km);
        a.reviser();
        Assert.assertEquals(a.kilometrage(),500,0.1);
    }

    @Test
    public void testRemiseNeg(){
        Velo a = new Velo();
        double km = 600.0;
        a.parcourir(km);
        Assert.assertEquals(a.prochaineRevision(),-100,0.1);
    }

    @Test
    public void testDecrocher(){
        Velo a = new Velo();
        a.arrimer();
        Assert.assertEquals(a.decrocher(),0);
    }

    @Test
    public void testDejaDecrocher(){
        Velo a = new Velo();
        Assert.assertEquals(a.decrocher(),-1);
    }

    @Test
    public void testReparer0(){
        Velo a = new Velo();
        a.abimer();
        Assert.assertEquals(a.reparer(),0);
    }

    @Test
    public void testReparer1(){
        Velo a = new Velo();
        a.arrimer();
        Assert.assertEquals(a.reparer(),-1);
    }

    @Test
    public void testReparer2(){
        Velo a = new Velo();
        a.decrocher();
        Assert.assertEquals(a.reparer(),-2);
    }

    @Test
    public void testReviser1(){
        Velo a = new Velo();
        a.arrimer();
        Assert.assertEquals(a.reviser(),-1);
    }

    @Test
    public void testReviser0(){
        Velo a = new Velo();
        Assert.assertEquals(a.reviser(),0);
    }

    @Test
    public void testReviser0Abime(){
        Velo a = new Velo();
        a.abimer();
        Assert.assertEquals(a.reviser(),0);
    }

    @Test
    public void testAbimer(){
        Velo a = new Velo();
        a.abimer();
        Assert.assertEquals(a.reparer(),0);
    }

    @Test
    public void testEstAbimer(){
        Velo a = new Velo();
        a.abimer();
        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testStringf(){
        char t = 'f';
        Velo a = new Velo(t);
        Assert.assertTrue(a.toString().contains("femme -"));
    }

    @Test
    public void testStringfMaj(){
        char t = 'F';
        Velo a = new Velo(t);
        Assert.assertTrue(a.toString().contains("femme -"));
    }

    @Test
    public void testStringh(){
        char t = 'h';
        Velo a = new Velo(t);
        Assert.assertTrue(a.toString().contains("homme -"));
    }

    @Test
    public void testStringhMaj(){
        char t = 'H';
        Velo a = new Velo(t);
        Assert.assertTrue(a.toString().contains("homme -"));
    }

    @Test
    public void testStringm(){
        char t = 'r';
        Velo a = new Velo(t);
        Assert.assertTrue(a.toString().contains("mixte -"));
    }

    @Test
    public void testStringRevision(){
        char t = 'H';
        Velo a = new Velo(t);
        double km = 600.0;
        a.parcourir(km);
        Assert.assertTrue(a.toString().contains("(révision nécessaire)"));
    }

    @Test
    public void testString1(){
        char t = 'H';
        Velo a = new Velo(t);
        double km = 600.0;
        a.parcourir(km);
        Assert.assertEquals(a.toString(),"Vélo cadre homme - 600.0 km (révision nécessaire)");
    }

    @Test
    public void testString2(){
        char t = 'F';
        Velo a = new Velo(t);
        double km = 600.0;
        a.parcourir(km);
        Assert.assertEquals(a.toString(),"Vélo cadre femme - 600.0 km (révision nécessaire)");
    }

    @Test
    public void testString3(){
        char t = 'k';
        Velo a = new Velo(t);
        double km = 400.0;
        a.parcourir(km);
        Assert.assertEquals(a.toString(),"Vélo cadre mixte - 400.0 km");
    }

    @Test
    public void testVeloNoOpt(){
        Velo a = new Velo();
        Assert.assertEquals("Vélo cadre mixte - 0.0 km", a.toString());
    }

    @Test
    public void testToString500(){
        Velo a = new Velo();
        a.decrocher();
        a.parcourir(500);
        Assert.assertEquals("Vélo cadre mixte - 500.0 km (révision nécessaire)", a.toString());
    }

    @Test
    public void testToStringArrondieSup(){
        Velo a = new Velo();
        a.parcourir(15.16);
        Assert.assertEquals("Vélo cadre mixte - 15.2 km", a.toString());
    }

    @Test
    public void testToStringArrondieInf(){
        Velo a = new Velo();
        a.parcourir(15.22);
        Assert.assertEquals("Vélo cadre mixte - 15.2 km", a.toString());
    }

    @Test
    public void testToStringArrondie(){
        Velo a = new Velo();
        a.parcourir(48.15162342);
        Assert.assertEquals("Vélo cadre mixte - 48.2 km", a.toString());
    }

    @Test
    public void testToStringArrondie500(){
        Velo a = new Velo();
        a.parcourir(499.99);
        Assert.assertEquals("Vélo cadre mixte - 500.0 km", a.toString());
    }

    @Test
    public void testToStringKMRevision(){
        Velo a = new Velo();
        a.parcourir(700);
        a.reviser();
        a.parcourir(200);
        Assert.assertEquals("Vélo cadre mixte - 900.0 km", a.toString());
    }

    @Test
    public void testAbimerTwoVelo(){
        Velo a = new Velo();
        a.abimer();
        a.abimer();
        Assert.assertTrue(a.estAbime());
    }

    @Test
    public void testReviserAccrocherKilometrage(){
        Velo a = new Velo();
        a.decrocher();
        a.parcourir(108);
        a.arrimer();
        a.reviser();
        Assert.assertEquals(108, a.kilometrage(), 0);
    }

    @Test
    public void testReviserKilometrageAccrocher(){
        Velo a = new Velo();
        a.decrocher();
        a.arrimer();
        a.parcourir(108);
        a.reviser();
        Assert.assertEquals(0, a.kilometrage(), 0);
    }
}
