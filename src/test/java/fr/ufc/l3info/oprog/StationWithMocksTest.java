package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class StationWithMocksTest {
    @Test
    public void testEmpruntAbonneBloque() {
        // instanciation du mock
        Abonne mockAbonne = Mockito.mock(Abonne.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockAbonne.estBloque()).thenReturn(true);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(mockRegistre);
        Assert.assertNull(s.emprunterVelo(mockAbonne, 1));
    }

    @Test
    public void testEmpruntRegNull() {
        // instanciation du mock
        Abonne mockAbonne = Mockito.mock(Abonne.class);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertNull(s.emprunterVelo(mockAbonne, 1));
    }

    @Test
    public void testEmpruntEnCoursNull() {
        // instanciation du mock
        Abonne mockAbonne = Mockito.mock(Abonne.class);
        //IVelo mockIVelo = Mockito.mock(IVelo.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockAbonne.estBloque()).thenReturn(false);
        Mockito.when(mockRegistre.nbEmpruntsEnCours(mockAbonne)).thenReturn(1);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(mockRegistre);
        Assert.assertNull(s.emprunterVelo(mockAbonne, 1));
    }

    @Test
    public void testEmpruntOk() {
        // instanciation du mock
        Abonne mockAbonne = Mockito.mock(Abonne.class);
        IVelo mockIVelo = Mockito.mock(IVelo.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockAbonne.estBloque()).thenReturn(false);
        // objet sous test
        Mockito.when(mockRegistre.retourner(eq(mockIVelo), Mockito.anyLong())).thenReturn(0);
        Station s = new Station("ok",0,0,10);
        s.setRegistre(mockRegistre);
        s.arrimerVelo(mockIVelo, 1);
        Assert.assertEquals(mockIVelo,s.emprunterVelo(mockAbonne, 1));
    }

    /*@Test
    public void testArrimerInf1() {
        // instanciation du mock
        IVelo mockIVelo = Mockito.mock(IVelo.class);
        // définition du comportement attendu pour l'objet
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertEquals(-1,s.arrimerVelo(mockIVelo, 0));
    }

    @Test
    public void testArrimerSupCap() {
        // instanciation du mock
        IVelo mockIVelo = Mockito.mock(IVelo.class);
        // définition du comportement attendu pour l'objet
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertEquals(-1,s.arrimerVelo(mockIVelo, 11));
    }*/

    @Test
    public void testArrimerVeloNull() {
        // instanciation du mock
        // définition du comportement attendu pour l'objet
        // objet sous test
        Station s = new Station("ok",0,0,10);
        Assert.assertEquals(-1,s.arrimerVelo(null, 1));
    }

    @Test
    public void testArrimerRegRetourner() {
        // instanciation du mock
        IVelo mockIVelo = Mockito.mock(IVelo.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockRegistre.retourner(eq(mockIVelo), Mockito.anyLong())).thenReturn(-1);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(mockRegistre);
        Assert.assertEquals(-4,s.arrimerVelo(mockIVelo, 1));
    }

    /*@Test
    public void testArrimerRegNull() {
        // instanciation du mock
        IVelo mockIVelo = Mockito.mock(IVelo.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockRegistre.retourner(eq(mockIVelo), Mockito.anyLong())).thenReturn(-1);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(null);
        Assert.assertEquals(-2,s.arrimerVelo(mockIVelo, 1));
    }*/

    @Test
    public void testArrimerOk() {
        // instanciation du mock
        IVelo mockIVelo = Mockito.mock(IVelo.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockRegistre.retourner(eq(mockIVelo), Mockito.anyLong())).thenReturn(0);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(mockRegistre);
        Assert.assertEquals(0,s.arrimerVelo(mockIVelo, 1));
    }

    @Test
    public void testArrimerDistance() {
        Station s = new Station("ok",10.0,20.0,10);
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertEquals(s1.distance(s),s.distance(s1),0.001);
    }

    @Test
    public void testArrimerDistanceSame() {
        Station s = new Station("ok",10.0,20.0,10);
        Station s1 = new Station("ok",10.0,20.0,10);
        Assert.assertEquals(0,s.distance(s1),0.001);
    }

    @Test
    public void testArrimerDistance100() {
        Station s = new Station("ok",10.0,20.0,10);
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertEquals(1111.9,s.distance(s1),0.1);
    }

    @Test
    public void testNom() {
        String ch = "ok";
        Station s1 = new Station(ch,0.0,20.0,10);
        Assert.assertEquals(ch, s1.getNom());
    }

    @Test
    public void testALaBorneSup() {
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertNull(s1.veloALaBorne(11));
    }

    @Test
    public void testALaBorneInf() {
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertNull(s1.veloALaBorne(0));
    }

    @Test
    public void testBorneLibre10() {
        Station s1 = new Station("ok",0.0,20.0,10);
        Assert.assertEquals(10,s1.nbBornesLibres());
    }

    @Test
    public void testBorneLibreZero() {
        Station s1 = new Station("ok",0.0,20.0,0);
        Assert.assertEquals(0,s1.nbBornesLibres());
    }

    @Test
    public void testBorneLibreNotNull() {
        // instanciation du mock
        IVelo mockIVelo = Mockito.mock(IVelo.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockRegistre.retourner(eq(mockIVelo), Mockito.anyLong())).thenReturn(0);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(mockRegistre);
        s.arrimerVelo(mockIVelo, 1);
        Assert.assertEquals(9,s.nbBornesLibres());
    }

    /*@Test
    public void testArrimerBPris() {
        // instanciation du mock
        IVelo mockIVelo1 = Mockito.mock(IVelo.class);
        IVelo mockIVelo2 = Mockito.mock(IVelo.class);
        IRegistre mockRegistre = Mockito.mock(IRegistre.class);
        // définition du comportement attendu pour l'objet
        Mockito.when(mockRegistre.retourner(eq(mockIVelo1), Mockito.anyLong())).thenReturn(0);
        // objet sous test
        Station s = new Station("ok",0,0,10);
        s.setRegistre(mockRegistre);
        s.arrimerVelo(mockIVelo2, 1);
        Assert.assertEquals(-2,s.arrimerVelo(mockIVelo1, 1));
    }*/
}