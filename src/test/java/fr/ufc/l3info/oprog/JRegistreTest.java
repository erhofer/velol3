package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

public class JRegistreTest {
    @Test
    public void testEmpruntAbonneNull() {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Assert.assertEquals(-1,r.emprunter(null,v,1));
    }
    @Test
    public void testEmpruntVeloNull() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        IRegistre r = new JRegistre();
        Assert.assertEquals(-1,r.emprunter(a,null,1));
    }
    @Test
    public void testEmpruntEnCours() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,1);
        Assert.assertEquals(-2,r.emprunter(a,v,1));
    }
    @Test
    public void testEmpruntOk() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Assert.assertEquals(0,r.emprunter(a,v,1));
    }
    /*--------*/
    @Test
    public void testRetournerVeloNull() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        IRegistre r = new JRegistre();
        Assert.assertEquals(-1,r.retourner(null,1));
    }
    @Test
    public void testRetournerSansEmprunt() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Assert.assertEquals(-2,r.retourner(v,1));
    }
    @Test
    public void testRetournerAvecEmpruntEnCoursOk() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,1);
        Assert.assertEquals(0,r.retourner(v,1));
    }
    @Test
    public void testRetournerAprèsRetour() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,1);
        r.retourner(v,1);
        Assert.assertEquals(-2,r.retourner(v,1));
    }
    @Test
    public void testRetournerMoins3() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,25);
        r.retourner(v,10);
        Assert.assertEquals(-3,r.retourner(v,10),0.01);
    }
    /*--------*/
    @Test
    public void testNbEmpruntVide() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        IRegistre r = new JRegistre();
        Assert.assertEquals(0,r.nbEmpruntsEnCours(a));
    }
    @Test
    public void testNbEmpruntApresDepot() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,1);
        r.retourner(v,1);
        Assert.assertEquals(0,r.nbEmpruntsEnCours(a));
    }
    @Test
    public void testNbEmpruntUn() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,1);
        Assert.assertEquals(1,r.nbEmpruntsEnCours(a));
    }
    @Test
    public void testNbEmpruntDeux() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IVelo v1 = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,1);
        r.emprunter(a,v1,1);
        Assert.assertEquals(2,r.nbEmpruntsEnCours(a));
    }
    @Test
    public void testNbEmpruntMeme() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,1);
        r.emprunter(a,v,1);
        Assert.assertEquals(1,r.nbEmpruntsEnCours(a));
    }
    /*--------*/
    @Test
    public void testFacturationAbonneNull() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Assert.assertEquals(0,r.facturation(a,15,25),0.01);
    }
    @Test
    public void testFacturation() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,20);
        r.retourner(v,1);
        Assert.assertEquals(0,r.facturation(a,15,25),0.01);
    }
    @Test
    public void testFacturation1() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,15);
        r.retourner(v,1);
        Assert.assertEquals(0,r.facturation(a,15,25),0.01);
    }
    /*--------*/
    @Test
    public void testEmprunterAbonne() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,15);
        Assert.assertEquals(a,r.emprunter(v));
    }

    @Test
    public void testEmprunterAbonneBloque() throws IncorrectNameException {
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        Assert.assertNull(r.emprunter(v));
    }

    @Test
    public void testEmprunterMaisVeloCasser() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        v.abimer();
        Assert.assertEquals(-2, r.emprunter(a,v,15));
    }

    @Test
    public void testRetournerVeloCasserAbonnePrison() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,15);
        v.abimer();
        r.retourner(v, 15);
        Assert.assertTrue(a.estBloque());
    }

    /*---*/



    @Test
    public void testTarifNormal() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v = fab.construire('H',"");
        IRegistre r = new JRegistre();
        r.emprunter(a,v,0);
        r.retourner(v, 3600000);

        Assert.assertEquals( 2.0,r.facturation(a,0,3600000),0.001);
    }

}
