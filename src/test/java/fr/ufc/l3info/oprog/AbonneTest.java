package fr.ufc.l3info.oprog;


import org.junit.Assert;
import org.junit.Test;

/**
 * Test unitaire pour les abonnés. 
 */
public class AbonneTest {
    
    @Test
    public void testNom() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred");
        // vérification de son nom
        Assert.assertEquals("Fred", a.getNom());
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomFalse() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("56fdsfds");
    }

    @Test
    public void testRibBloque() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fredousosu", "00000-97");
        Assert.assertTrue(a.estBloque());
    }
    @Test
    public void testWRibBloque() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fredousosu", "00000-00000-00000000100-94");
        a.bloquer();
        a.debloquer();
        Assert.assertTrue(a.estBloque());
    }
    @Test
    public void testCourtRibBloque() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fredousosu", "00000000100-94");
        a.bloquer();
        a.debloquer();
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testNomBloque() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fredousosu", "00000-00000-00000000000-94");
        Assert.assertTrue(a.estBloque());
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomVide() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomSpace() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("fdnsk  fds fds");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomTiret() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("fdnsk--fds");
    }

    @Test
    public void testNomTiretok() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("fdnsk-fds");
        // vérification de son nom
        Assert.assertEquals("fdnsk-fds", a.getNom());
    }

    @Test
    public void testNomTrim() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("   Fred   ");
        // vérification de son nom
        Assert.assertEquals("Fred", a.getNom());
    }

    @Test
    public void testNomSpaceok() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fr ed");
        // vérification de son nom
        Assert.assertEquals("Fr ed", a.getNom());
    }

    @Test
    public void testRib() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        //vérification de son rib
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testRibNom() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        //vérification de son nom
        Assert.assertEquals("Fred", a.getNom());
    }

    @Test
    public void testNewRib() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-87");
        //vérification de son rib
        a.miseAJourRIB("00000-00000-00000000003-88");
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testNewRibF() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-87");
        //vérification de son rib
        a.miseAJourRIB("00000-00000-00000000000-97");
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testNewRibWrong() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-87");
        //vérification de son rib
        a.miseAJourRIB("00000-00000-00000050000-97");
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testNewRibCourt() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-87");
        //vérification de son rib est tjs bloqué
        a.miseAJourRIB("00000-00000-78");

        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testEqualsT() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        Assert.assertTrue(a.equals(a));
    }

    @Test
    public void testEqualsF() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        Abonne b = new Abonne("Hugo", "00000-00000-00000000003-88");
        Assert.assertFalse(a.equals(b));
    }

    @Test
    public void testHashCodeDiff() throws IncorrectNameException{
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        // création d'un nouvel abonné
        Abonne b = new Abonne("Hugo", "00000-00000-00000000003-88");
        Assert.assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void testEquals() throws IncorrectNameException{
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        Object b = new Object();
        Assert.assertFalse(a.equals(b));
    }

    @Test
    public void testSameHashCode() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        Assert.assertEquals(a.hashCode(), a.hashCode());
    }

    @Test
    public void testID() throws IncorrectNameException{
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        // création d'un nouvel abonné
        Abonne b = new Abonne("Hugol", "00000-00000-00000000003-88");
        System.out.println("get id : "+a.getID());
        System.out.println("get id : "+b.getID());
        Assert.assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void testIDsame() throws IncorrectNameException{
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", "00000-00000-00000000000-97");
        System.out.println("get id : "+a.getID());
        Assert.assertEquals(a.hashCode(), a.hashCode());
    }

    @Test
    public void testBloque() throws IncorrectNameException{
        Abonne a = new Abonne("Fred", "00000-00000-00000000003-88");
        a.bloquer();
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testDebloque() throws IncorrectNameException{
        Abonne a = new Abonne("Fred", "00000-00000-00000000003-88");
        a.debloquer();
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testNotBloque() throws IncorrectNameException{
        Abonne a = new Abonne("Fred", "00000-00000-00000000003-88");
        Assert.assertFalse(a.estBloque());
    }
}
