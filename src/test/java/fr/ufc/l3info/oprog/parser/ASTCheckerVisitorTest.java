package fr.ufc.l3info.oprog.parser;
import fr.ufc.l3info.oprog.Station;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ASTCheckerVisitorTest {
    final String path = "./target/classes/data/";

    /** Instance singleton du parser de stations */
    final StationParser parser = StationParser.getInstance();

    @Test
    public void testStationVide() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest1.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        v.visit(n);
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.EMPTY_LIST));
    }

    @Test
    public void testStationOk() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationsOK.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        v.visit(n);
        Assert.assertTrue(v.getErrors().isEmpty());
    }

    @Test
    public void testStationNomDouble() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest2.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.DUPLICATE_STATION_NAME));
    }

    @Test
    public void testStationNomVide() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest3.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.getChild(0).getChild(0).accept(v);
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.EMPTY_STATION_NAME));
    }

    @Test
    public void testStationNomSpace() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest3.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.getChild(1).getChild(0).accept(v);
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.EMPTY_STATION_NAME));
    }

    @Test
    public void testStationNomTrim() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest4.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.getChild(5).getChild(0).accept(v);
        Assert.assertTrue(v.getErrors().isEmpty());
    }

    @Test
    public void testStationNombreNeg() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest4.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();

        //System.out.println(n.getChild(0).getChild(i).getChild(0).value);
        for (int i = 1 ; i < n.getChild(0).getNumChildren(); i++) {
            if (n.getChild(0).getChild(i).getChild(0).value.equals("capacite")){
                n.getChild(0).getChild(i).getChild(1).accept(v);
            }
        }
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.WRONG_NUMBER_VALUE));
    }

    @Test
    public void testStationNombreDecimal() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest4.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();

        //System.out.println(n.getChild(1).value);
        for (int i = 1 ; i < n.getChild(1).getNumChildren(); i++) {
            if (n.getChild(1).getChild(i).getChild(0).value.equals("capacite")){
                n.getChild(1).getChild(i).getChild(1).accept(v);
            }
        }
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.WRONG_NUMBER_VALUE));
    }

    @Test
    public void testStationNombreNoDoubleDec() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest4.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();

        //System.out.println(n.getChild(1).value);
        n.getChild(1).accept(v);
        Assert.assertTrue(v.getErrors().isEmpty());
    }

    @Test
    public void testStationNombreDoubleDec() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest4.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();

        //System.out.println(n.getChild(1).value);
        n.getChild(2).accept(v);
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.DUPLICATE_DECLARATION));
    }

    @Test
    public void testStationNombreManqueDec() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest4.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();

        //System.out.println(n.getChild(1).value);
        n.getChild(3).accept(v);
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.MISSING_DECLARATION));
    }

    @Test
    public void testStationNombreManqueDeux() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/stationtest4.txt"));
        ASTCheckerVisitor v = new ASTCheckerVisitor();

        //System.out.println(n.getChild(1).value);
        n.getChild(4).accept(v);
        System.out.println(v.getErrors());
        //Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.MISSING_DECLARATION));
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.DUPLICATE_DECLARATION));
    }
}
